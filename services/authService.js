const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search({ email: userData.email });
        if (!user) {
            throw Error(`User with this email(${userData.email}) not found`);
        } else if (user.password !== userData.password) {
            throw Error(`Wrong password.`);
        } else {
            return user;
        }
    }
}

module.exports = new AuthService();