const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    getAll() {
        const items = FightRepository.getAll();
        if (items.length === 0) return null;

        return items;
    };

    search(search) {
        const item = FightRepository.getOne(search);
        if (!item) return null;

        return item;
    };

    create(data) {
        data.log = [];
        const item = FightRepository.create(data);
        if (!item) throw Error('Unable to create fight.')

        return item;
    }

    update(id, data) {
        const fights = this.search({ id: id });

        if (fights) {
            const item = FightRepository.update(id, data);
            return item;
        } else throw Error(`Fight with this id(${id}) not found.`)
    }

}

module.exports = new FightersService();