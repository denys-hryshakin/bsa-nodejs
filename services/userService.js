const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAll() {
        const items = UserRepository.getAll();
        if (items.length === 0) return null;

        return items;
    }

    create(data) {
        const users = this.getAll();

        // check if users exist in db
        if (users !== null) {
            const isEmailExists = users.map(item => item.email).includes(data.email);
            const isPhoneNumberExists = users.map(item => item.phoneNumber).includes(data.phoneNumber);

            // check for email and phone number exist
            if (isEmailExists) {
                throw Error(`User with this email (${data.email}) already exists.`);
            } else if (isPhoneNumberExists) {
                throw Error(`User with this phone number (${data.phoneNumber}) already exists.`);
            } else {
                const item = UserRepository.create(data);
                return item;
            }
        } else {
            const item = UserRepository.create(data);
            return item;
        }
    }

    update(id, data) {
        const allUsers = this.getAll();
        const reqUser = this.search({ id: id });

        const isEmailExists = allUsers.map(item => item.email).includes(data.email);
        const isPhoneNumberExists = allUsers.map(item => item.phoneNumber).includes(data.phoneNumber);

        if (reqUser) {
            if (isEmailExists && data.email !== reqUser.email) {
                throw Error(`User with this email (${data.email}) already exists.`);
            } else if (isPhoneNumberExists && data.phoneNumber !== reqUser.phoneNumber) {
                throw Error(`User with this phone number (${data.phoneNumber}) already exists.`);
            } else {
                const item = UserRepository.update(id, data);
                return item;
            }
        } else throw Error(`User not found (id: '${id}').`);
    }

    delete(id) {
        // check if user exist in db
        const isUserExist = this.search({ id: id });
        if (isUserExist === null) {
            throw Error(`User not found (id: '${id}').`);
        } else {
            const item = UserRepository.delete(id);
            return item;
        }
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) return null;

        return item;
    }
}

module.exports = new UserService();