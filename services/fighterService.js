const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAll() {
        const items = FighterRepository.getAll();
        if (items.length === 0) return null;

        return items;
    }

    create(data) {
        const items = this.getAll();

        // check for fighters in the database
        if (items !== null) {
            const isNameExists = items.map(item => item.name).includes(data.name);

            // check if name exist
            if (isNameExists) {
                throw Error(`Fighter with this name (${data.name}) already exists.`);
            } else {
                const item = FighterRepository.create(data);
                return item;
            }
        } else {
            const item = FighterRepository.create(data);
            return item;
        }
    }

    update(id, data) {
        const allFighters = this.getAll();
        const reqFighter = this.search({ id: id });

        const isNameExists = allFighters.map(item => item.name).includes(data.name)

        if (reqFighter) {
            if (isNameExists && data.name !== reqFighter.name) {
                throw Error(`Fighter with this name (${data.name}) already exists.`);
            } else {
                const item = FighterRepository.update(id, data);
                return item;
            }
        } else throw Error(`Fighter not found (id: '${id}').`);
    }

    delete(id) {
        // check if fighter exist in db
        const isFigherExist = this.search({ id: id });
        if (isFigherExist === null) {
            throw Error(`Fighter not found (id: '${id}').`);
        } else {
            const item = FighterRepository.delete(id);
            return item;
        }
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) return null;

        return item;
    }
}

module.exports = new FighterService();