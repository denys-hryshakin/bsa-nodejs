const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// OPTIONAL TODO: Implement route controller for fights

// get all fights
router.get('/', function (req, res, next) {
    try {
        const fights = FightService.getAll();
        if (fights) res.data = fights;
        else throw Error('Fights not found.')
    } catch (error) {
        res.error = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// get fight by id
router.get('/:id', function (req, res, next) {
    try {
        const item = FightService.search({ id: req.params.id });
        if (item) res.data = item;
        else throw Error(`Fight not found (id: '${req.params.id}').`);
    } catch (error) {
        res.error = error.toString();
    } finally {
        next()
    }
}, responseMiddleware);

// create and save fight
router.post('/', function (req, res, next) {
    try {
        const fight = FightService.create(req.body);
        res.data = fight;
    } catch (error) {
        res.error = error.toString();
    } finally {
        next()
    }
}, responseMiddleware);

// update fights' log
router.put('/:id', function (req, res, next) {
    try {
        const fight = FightService.update(req.params.id, req.body);
        res.data = fight;
    } catch (error) {
        res.error = error.toString();
    } finally {
        next()
    }
}, responseMiddleware);

module.exports = router;