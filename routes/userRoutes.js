const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

// Get all users
router.get('/', function (req, res, next) {
    try {
        const users = UserService.getAll();
        if (users) res.data = users;
        else throw Error('Users not found.');
    } catch (error) {
        res.notFoundError = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Get user by id
router.get('/:id', function (req, res, next) {
    try {
        const user = UserService.search({ id: req.params.id });
        if (user) res.data = user;
        else throw Error(`User not found (id: '${req.params.id}').`);
    } catch (error) {
        res.notFoundError = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Create user
router.post('/', createUserValid, responseMiddleware, function (req, res, next) {
    try {
        const user = UserService.create(req.body);
        res.data = user;
    } catch (error) {
        res.error = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Update user by id
router.put('/:id', updateUserValid, responseMiddleware, function (req, res, next) {
    try {
        const user = UserService.update(req.params.id, req.body);
        res.data = user;
    } catch (error) {
        res.error = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Delete user by id
router.delete('/:id', function (req, res, next) {
    try {
        const user = UserService.delete(req.params.id);
        res.data = user;
    } catch (error) {
        res.notFoundError = error.toString();
    }
    finally {
        next();
    }
}, responseMiddleware);

module.exports = router;