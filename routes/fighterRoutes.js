const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

// Get all fighters
router.get('/', function (req, res, next) {
    try {
        const items = FighterService.getAll();
        if (items) res.data = items;
        else throw Error('Fighters not found.');
    } catch (error) {
        res.notFoundError = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Get fighter by id
router.get('/:id', function (req, res, next) {
    try {
        const item = FighterService.search({ id: req.params.id });
        if (item) res.data = item;
        else throw Error(`Fighter not found (id: '${req.params.id}').`);
    } catch (error) {
        res.notFoundError = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Create fighter
router.post('/', createFighterValid, responseMiddleware, function (req, res, next) {
    try {
        const fighter = FighterService.create(req.body);
        res.data = fighter;
    } catch (error) {
        res.error = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Update fighter by id
router.put('/:id', updateFighterValid, responseMiddleware, function (req, res, next) {
    try {
        const fighter = FighterService.update(req.params.id, req.body);
        res.data = fighter;
    } catch (error) {
        res.error = error.toString();
    } finally {
        next();
    }
}, responseMiddleware);

// Delete fighter by id
router.delete('/:id', function (req, res, next) {
    try {
        const fighter = FighterService.delete(req.params.id);
        res.data = fighter;
    } catch (error) {
        res.notFoundError = error.toString();
    }
    finally {
        next();
    }
}, responseMiddleware);

module.exports = router;