const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const userBody = req.body;

    // Break object (key: value) on array of keys
    const userBodyKeys = Object.keys(userBody);
    const userModelKeys = Object.keys(user).filter(key => key !== 'id'); // without id

    // Now we can compare arrays using method ES7 includes()
    const validData = userModelKeys.filter(key => userBodyKeys.includes(key)); // check bodyData == modelData
    const invalidData = userBodyKeys.filter(key => !userModelKeys.includes(key));

    //test input data with regular expression in model
    const RegExpCheck = userModelKeys.every(key => user[key].test(userBody[key]));

    if (validData.length === userModelKeys.length && invalidData.length === 0 && RegExpCheck) {
        next();
    } else if (validData.length < userModelKeys.length) {
        res.error = `Unable to create user without: ${userModelKeys.filter(key => !validData.includes(key)).toString()}`;
    } else if (invalidData.length !== 0) {
        res.error = `Unable to create user with invalid data (${invalidData.toString()}).`;
    } else {
        let invalidFields = [];
        userModelKeys.forEach(key => {
            if (!user[key].test(userBody[key])) {
                invalidFields.push(key);
            }
        });
        res.error = `Unable to create user. Invalid fields (${invalidFields}).`;
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const userBody = req.body;

    // Break object (key: value) on array of keys
    const userBodyKeys = Object.keys(userBody);
    const userModelKeys = Object.keys(user).filter(key => key !== 'id'); // without id

    // Now we can compare arrays using method ES7 includes()
    const validData = userModelKeys.filter(key => userBodyKeys.includes(key)); // check bodyData == modelData
    const invalidData = userBodyKeys.filter(key => !userModelKeys.includes(key));

    //test input data with regular expression in model
    const RegExpCheck = validData.every(key => user[key].test(userBody[key]));

    if (validData.length !== 0 && invalidData.length === 0 && RegExpCheck) {
        next();
    } else if (invalidData.length !== 0) {
        res.error = `Unable to update user with invalid data (${invalidData.toString()}).`;
    } else {
        let invalidFields = [];
        userModelKeys.forEach(key => {
            if (!user[key].test(userBody[key])) {
                invalidFields.push(key)
            }
        });
        res.error = `Unable to create user. Invalid fields (${invalidFields}).`;
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;