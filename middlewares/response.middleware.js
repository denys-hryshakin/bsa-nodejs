const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.error) {
        res.status(400).json({ error: true, message: res.error });
    } else if (res.notFoundError) {
        res.status(404).json({ error: true, message: res.notFoundError });
    } else if (res.data) {
        res.status(200).json(res.data);
    } else next();
}

exports.responseMiddleware = responseMiddleware;