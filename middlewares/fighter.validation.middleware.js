const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const fighterBody = req.body;

    // health by default = 100, if fighter skip it
    if (!fighterBody.health) fighterBody.health = 100;

    // Break object (key: value) on array of keys
    const fighterBodyKeys = Object.keys(fighterBody);
    const fighterModelKeys = Object.keys(fighter).filter(key => key !== 'id'); // without id

    // Now we can compare arrays using method ES7 includes()
    const validData = fighterModelKeys.filter(key => fighterBodyKeys.includes(key)); // check bodyData == modelData
    const invalidData = fighterBodyKeys.filter(key => !fighterModelKeys.includes(key));

    //test input data with regular expression in model
    const RegExpCheck = fighterModelKeys.every(key => fighter[key].test(fighterBody[key]));

    if (validData.length === fighterModelKeys.length && invalidData.length === 0 && RegExpCheck) {
        next();
    } else if (validData.length < fighterModelKeys.length) {
        res.error = `Unable to create fighter without: ${fighterModelKeys.filter(key => !validData.includes(key)).toString()}`;
    } else if (invalidData.length !== 0) {
        res.error = `Unable to create fighter with invalid data (${invalidData.toString()}).`;
    } else {
        let invalidFields = [];
        fighterModelKeys.forEach(key => {
            if (!fighter[key].test(fighterBody[key])) {
                invalidFields.push(key);
            }
        });
        res.error = `Unable to create fighter. Invalid fields (${invalidFields}). Name(unique), Power(1-100), Defense(1-10), Health(80-120, default:100)`;
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const fighterBody = req.body;

    // Break object (key: value) on array of keys
    const fighterBodyKeys = Object.keys(fighterBody);
    const fighterModelKeys = Object.keys(fighter).filter(key => key !== 'id'); // without id

    // Now we can compare arrays using method ES7 includes()
    const validData = fighterModelKeys.filter(key => fighterBodyKeys.includes(key)); // check bodyData == modelData
    const invalidData = fighterBodyKeys.filter(key => !fighterModelKeys.includes(key));

    //test input data with regular expression in model
    const RegExpCheck = validData.every(key => fighter[key].test(fighterBody[key]));

    if (validData.length !== 0 && invalidData.length === 0 && RegExpCheck) {
        next();
    } else if (invalidData.length !== 0) {
        res.error = `Unable to update fighter with invalid data (${invalidData.toString()}).`;
    } else {
        let invalidFields = [];
        fighterModelKeys.forEach(key => {
            if (!fighter[key].test(fighterBody[key])) {
                invalidFields.push(key)
            }
        });
        res.error = `Unable to create fighter. Invalid fields (${invalidFields}).`;
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;