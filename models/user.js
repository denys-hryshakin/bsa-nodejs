exports.user = {
    id: '',
    firstName: /[a-z]$/i,
    lastName: /[a-z]$/i,
    email: /[a-z0-9]+@gmail\.com$/i,
    phoneNumber: /\+380+[0-9]{9}$/i,
    password: /(?=.*[0-9!@#$%^&*])(?=.*[a-z])[0-9a-zA-Z!@#$%^&*]{3,}/ // min 3 symbols
}